(function() {
  var app = angular.module('store', []);

  app.controller('StoreController', function() {
    this.products = gems;
  });

  var gems = [{
    name: "Dodecahedron",
    price: 2.95,
    description: "Nice stone",
    canPurchase: true,
    soldOut: false,
    date: 13000000000,
    images: [
      "img/gem1.jpg",
      "img/gem2.png"
    ],
    shine: 3,
    reviews: [{
      stars: 3,
      body: "I think this gem was just OK, could honestly use more shine, IMO.",
      author: "JimmyDean@example.org",
      createdOn: 1397490980837
    }, {
      stars: 4,
      body: "Any gem with 12 faces is for me!",
      author: "gemsRock@example.org",
      createdOn: 1397490980837
    }]
  },
  {
    name: "Pentagonal Gem",
    price: 5.95,
    description: "The best stone",
    canPurchase: false,
    soldOut: false,
    date: 14000000000,
    images: [
      "img/gem3.jpg",
      "img/gem2.png"
    ],
    shine: 2,
    reviews: [{
        stars: 5,
        body: "I love this gem!",
        author: "joe@example.org",
        createdOn: 1397490980837
      }, {
        stars: 1,
        body: "This gem sucks.",
        author: "tim@example.org",
        createdOn: 1397490980837
      }]
  }];

  // tab
  app.controller("TabController", function() {
    this.tab = 1;

    this.setTab = function(q) {
      this.tab = q;
    };

    this.isSet = function(iq) {
      return iq === this.tab;
    };
  });

  // gallery
  app.controller("GalleryController", function() {
    this.current = 0;

    this.setCurrent = function(iq) {
      if(iq) {
        this.current = iq;
      } else {
        this.current = 0;
      }
    };
  });

  // review
  app.controller("RewievController", function() {
    this.review = {};

    this.addReview = function(product) {
      product.reviews.push(this.review);
      this.review = {};
    };
  });

})();
